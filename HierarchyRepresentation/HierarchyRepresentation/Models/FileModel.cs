﻿using HierarchyRepresentation.Attributes;
using Newtonsoft.Json;

namespace HierarchyRepresentation.Models
{
    public class FileModel
    {
        public string Name { get; set; }
        [JsonConverter(typeof(CustomLongJsonConverter))]
        public long Size { get; set; }
        public string Path { get; set; }
    }
}