﻿using HierarchyRepresentation.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace HierarchyRepresentation.Models
{
    public class DirectoryModel
    {
        public string Name { get; set; }
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime DateCreated { get; set; }
        public List<FileModel> Files { get; set; } = new List<FileModel>();
        public List<DirectoryModel> Children { get; set; } = new List<DirectoryModel>();
    }
}