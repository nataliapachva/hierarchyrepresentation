﻿using Newtonsoft.Json;
using System;

namespace HierarchyRepresentation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input path");
            //string path = @"D:\Natalia";
            string path = Console.ReadLine();
            var representation = new RepresentationDirectoryTree();
            var directory = representation.GetDirectoryTree(path);
            var json = JsonConvert.SerializeObject(directory);
            Console.WriteLine(json);
            Console.ReadKey();
        }
    }
}