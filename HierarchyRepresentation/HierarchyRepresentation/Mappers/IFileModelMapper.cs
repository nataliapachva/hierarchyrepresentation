﻿using HierarchyRepresentation.Models;
using System.IO;

namespace HierarchyRepresentation.Mappers
{
    public interface IFileModelMapper
    {
        FileModel Map(FileInfo fileInfo);
    }
}