﻿using HierarchyRepresentation.Models;
using System.IO;

namespace HierarchyRepresentation.Mappers
{
    public class DirectoryModelMapper: IDirectoryModelMapper
    {
        public DirectoryModel Map(DirectoryInfo directoryInfo)
        {
            var directory = new DirectoryModel
            {
                Name = directoryInfo.Name,
                DateCreated = directoryInfo.CreationTime
            };

            return directory;
        }
    }
}