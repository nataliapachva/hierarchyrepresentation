﻿using HierarchyRepresentation.Models;
using System.IO;

namespace HierarchyRepresentation.Mappers
{
    public class FileModelMapper : IFileModelMapper
    {
        public FileModel Map(FileInfo fileInfo)
        {
            var fileModel = new FileModel { Name = fileInfo.Name, Path = fileInfo.FullName, Size = fileInfo.Length };

            return fileModel;
        }
    }
}