﻿using HierarchyRepresentation.Models;
using System.IO;

namespace HierarchyRepresentation.Mappers
{
    public interface IDirectoryModelMapper
    {
        DirectoryModel Map(DirectoryInfo directoryInfo);
    }
}