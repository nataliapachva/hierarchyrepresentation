﻿using Newtonsoft.Json.Converters;

namespace HierarchyRepresentation.Attributes
{
    public class CustomDateTimeConverter : IsoDateTimeConverter
    {
        public CustomDateTimeConverter()
        {
            base.DateTimeFormat = "dd-MMM-yyyy hh:mm tt";
        }
    }
}