﻿using HierarchyRepresentation.Mappers;
using HierarchyRepresentation.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace HierarchyRepresentation
{
    public class RepresentationDirectoryTree
    {
        private readonly IDirectoryModelMapper directoryModelMapper;
        private readonly IFileModelMapper fileModelMapper;

        public RepresentationDirectoryTree(): this(new DirectoryModelMapper(), new FileModelMapper())
        {}

        public RepresentationDirectoryTree(IDirectoryModelMapper directoryModelMapper, IFileModelMapper fileModelMapper)
        {
            this.directoryModelMapper = directoryModelMapper;
            this.fileModelMapper = fileModelMapper;
        }

        public DirectoryModel GetDirectoryTree(string path)
        {
            var directoryInfo = new DirectoryInfo(path);
            var directory = directoryModelMapper.Map(directoryInfo);

            try
            {
                var directories = GetDirectories(path);
                directory.Children.AddRange(directories);
                var files = GetFiles(path);
                directory.Files.AddRange(files);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return directory;
        }

        private List<DirectoryModel> GetDirectories(string path)
        {
            var directories = new List<DirectoryModel>();
            foreach (string d in Directory.GetDirectories(path))
            {
                var item = GetDirectoryTree(d);
                directories.Add(item);
            }

            return directories;
        }

        private List<FileModel> GetFiles(string path)
        {
            var files = new List<FileModel>();
            foreach (var file in Directory.GetFiles(path))
            {
                var fileInfo = new FileInfo(file);
                var fileModel = fileModelMapper.Map(fileInfo);
                files.Add(fileModel);
            }

            return files;
        }
    }
}