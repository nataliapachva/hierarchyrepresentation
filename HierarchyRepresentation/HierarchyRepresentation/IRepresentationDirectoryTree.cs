﻿using HierarchyRepresentation.Models;

namespace HierarchyRepresentation
{
    public interface IRepresentationDirectoryTree
    {
        DirectoryModel GetDirectoryTree(string path);
    }
}